package net.diamondmine.mcftfill.listeners;

import static net.diamondmine.mcftfill.McftFill.log;

import java.util.HashMap;

import net.diamondmine.mcftfill.McftFill;
import net.diamondmine.mcftfill.block.BlockPair;
import net.milkbowl.vault.permission.Permission;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

/**
 * Player listener for block selects.
 * 
 * @author Jon la Cour
 * @version 1.0.2
 */
public class BlockSelect implements Listener {
    McftFill plugin;
    Permission p = McftFill.p;
    HashMap<String, BlockPair> blockSelection;

    /**
     * Block select function.
     * 
     * @param p
     *            Plugin class
     * @since 1.0.0
     */
    public BlockSelect(final McftFill p) {
        plugin = p;
        Bukkit.getServer().getPluginManager().registerEvents(this, p);
        blockSelection = new HashMap<String, BlockPair>();
    }

    /**
     * Check if a player has a block selection made.
     * 
     * @param player
     *            Player
     * @since 1.0.0
     */
    public final void check(final Player player) {
        String pname = player.getName();
        if (!blockSelection.containsKey(pname)) {
            blockSelection.put(pname, new BlockPair(player));
        }
    }

    /**
     * Checks a player's inventory.
     * 
     * @param player
     *            Player to check
     * @param type
     *            Type of block
     * @param data
     *            Data of block
     * @param amount
     *            Amount
     * @return boolean
     * @since 1.0.0
     */
    @SuppressWarnings("deprecation")
    public final boolean checkInventory(final Player player, final int type, final byte data, final int amount) {
        if (player.getGameMode() == GameMode.SURVIVAL) {
            int amountIn = amount;
            ItemStack[] stacks = player.getInventory().getContents().clone();
            for (int i = 0; i < stacks.length; i++) {
                ItemStack stack = stacks[i];
                if (amountIn == 0) {
                    break;
                }
                if (stack != null) {
                    int typeId = stack.getTypeId();
                    byte dataIn = 0;
                    if (stack.getData() != null) {
                        dataIn = stack.getData().getData();
                    }
                    int stackSize = stack.getAmount();
                    if (type == typeId && data == dataIn) {
                        if (amountIn - stackSize > 0) {
                            stacks[i] = null;
                            amountIn = amountIn - stackSize;
                        } else {
                            int size = stackSize - amountIn;
                            stacks[i] = new ItemStack(typeId, size, dataIn);
                            amountIn = 0;
                        }
                    }
                }
            }
            if (amountIn == 0) {
                player.getInventory().setContents(stacks);
                player.updateInventory();
                return true;
            }
            return false;
        } else {
            return true;
        }
    }

    /**
     * Player response for selection of area.
     * 
     * @param event
     *            The PlayerInteractEvent
     * @since 1.0.0
     */
    @EventHandler(priority = EventPriority.MONITOR)
    public final void onPlayerInteract(final PlayerInteractEvent event) {
        if (event.isCancelled()) {
            return;
        }
        Player player = event.getPlayer();
        String pname = player.getName();
        if (!McftFill.users.contains(pname)) {
            return;
        }
        if (player.getItemInHand().getTypeId() == plugin.toolId) {
            if (event.getAction() == Action.LEFT_CLICK_BLOCK) {
                if (player.getGameMode() == GameMode.CREATIVE) {
                    event.setCancelled(true);
                }
                check(player);
                BlockPair bs = blockSelection.get(pname);
                if (bs.rightMatch(event.getClickedBlock().getLocation())) {
                    String block = bs.getLeftClick().getBlock().getType().name().replace("_", " ").toLowerCase();
                    String world = player.getWorld().getName();
                    String group = p.getPrimaryGroup(world, pname).toLowerCase();
                    if (McftFill.limits.containsKey(group)) {
                        int limit = McftFill.limits.get(group);
                        if (limit >= 1) {
                            if (bs.getArea() > McftFill.limits.get(group)) {
                                player.sendMessage(ChatColor.GOLD + "The maximum allowed size of a fill for your group is " + ChatColor.YELLOW + McftFill.limits.get(group) + ChatColor.GOLD + " blocks");
                                return;
                            }
                        } else if (limit == 0) {
                            player.sendMessage(ChatColor.GOLD + "Your group is not allowed to fill!");
                            return;
                        }
                    } else {
                        player.sendMessage(ChatColor.GOLD + "Your group is not allowed to fill!");
                        return;
                    }
                    if (bs.getArea() > plugin.maxSize && plugin.maxSize != 0) {
                        blockSelection.remove(pname);
                        player.sendMessage(ChatColor.GOLD + "The maximum allowed size of a fill is " + ChatColor.YELLOW + plugin.maxSize + " blocks.");
                        return;
                    }
                    if (!checkInventory(player, bs.getLeftClick().getBlock().getTypeId(), bs.getLeftClick().getBlock().getData(), bs.getArea())) {
                        player.sendMessage(ChatColor.GOLD + "You do not have enough " + block + " in your inventory.");
                        return;
                    }
                    blockSelection.remove(pname);
                    bs.queue(plugin);
                    if (player.getGameMode() == GameMode.SURVIVAL) {
                        player.sendMessage(ChatColor.YELLOW + "" + bs.getArea() + " " + ChatColor.GOLD + block + " blocks were removed from your inventory for the fill.");
                    } else {
                        player.sendMessage(ChatColor.YELLOW + "" + bs.getArea() + ChatColor.GOLD + " blocks were filled with " + block + " blocks.");
                        int xloc = (int) bs.getLeftClick().getX();
                        int yloc = (int) bs.getLeftClick().getY();
                        int zloc = (int) bs.getLeftClick().getZ();
                        String location = " ([" + world + "] " + xloc + ", " + yloc + ", " + zloc + ")";
                        if (McftFill.limits.containsKey(group)) {
                            int limit = McftFill.limits.get(group);
                            if (limit >= 1) {
                                group = Character.toUpperCase(group.charAt(0)) + group.substring(1);
                                log(pname + " [" + group + "] made a fill of " + bs.getArea() + " " + block + " blocks, of their " + limit + " block limit." + location, "info");
                            } else if (limit == -1) {
                                group = Character.toUpperCase(group.charAt(0)) + group.substring(1);
                                log(pname + " [" + group + "] made a fill of " + bs.getArea() + " " + block + " blocks, they have no fill limit." + location, "info");
                            }
                        } else {
                            group = Character.toUpperCase(group.charAt(0)) + group.substring(1);
                            log(pname + " [" + group + "] made a fill of " + bs.getArea() + " " + block + " blocks, they have no fill limit." + location, "info");
                        }

                    }
                    return;
                }
                bs.setLeftClick(event.getClickedBlock().getLocation());
                String block = bs.getLeftClick().getBlock().getType().name().replace("_", " ").toLowerCase();
                block = Character.toUpperCase(block.charAt(0)) + block.substring(1);
                player.sendMessage(ChatColor.GOLD + "" + block + " block selected. Right click a block to complete the fill!");
            }

            if (event.getAction() == Action.RIGHT_CLICK_BLOCK) {
                check(player);
                BlockPair bs = blockSelection.get(player.getName());
                if (bs.getLeftClick() == null) {
                    player.sendMessage(ChatColor.GOLD + "Left click a block to begin the selection.");
                    return;
                }
                if (bs.rightMatch(event.getClickedBlock().getLocation()) && bs.getLeftClick() != null) {
                    blockSelection.remove(player.getName());
                    player.sendMessage(ChatColor.GOLD + "Selection removed.");
                    return;
                }
                bs.setRightClick(event.getClickedBlock().getLocation());
                player.sendMessage(ChatColor.YELLOW + "" + bs.getArea() + ChatColor.GOLD + " blocks selected. Left click this block to complete the fill or right click it to remove the selection.");
            }
        }
    }
}
