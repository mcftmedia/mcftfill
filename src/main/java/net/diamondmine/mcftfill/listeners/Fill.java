package net.diamondmine.mcftfill.listeners;

import net.diamondmine.mcftfill.block.BlockChange;

import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.plugin.Plugin;

/**
 * Block listener for fills.
 * 
 * @author Jon la Cour
 * @version 1.0.4
 */
public class Fill implements Listener {

    public Fill(final Plugin plugin) {
        Bukkit.getServer().getPluginManager().registerEvents(this, plugin);
    }

    /**
     * On block place listener.
     * 
     * @param event
     *            The BlockPlaceEvent
     * @since 1.0.0
     */
    @EventHandler(priority = EventPriority.MONITOR)
    public synchronized final void onBlockPlace(final BlockPlaceEvent event) {
        if (event.isCancelled()) {
            return;
        }

        if (event instanceof BlockPlace) {
            BlockChange bNext = ((BlockPlace) event).getBlockChange();
            bNext.getWorld().getBlockAt(bNext.getLocation()).setTypeIdAndData(bNext.getType(), bNext.getData(), true);
        }
    }
}
