package net.diamondmine.mcftfill.listeners;


import net.diamondmine.mcftfill.block.BlockChange;

import org.bukkit.craftbukkit.v1_5_R3.block.CraftBlock;
import org.bukkit.entity.Player;
import org.bukkit.event.block.BlockPlaceEvent;

/**
 * Block place listener.
 * 
 * @author Jon la Cour
 * @version 1.2.0
 */
public class BlockPlace extends BlockPlaceEvent {
    private final BlockChange bc;

    /**
     * Block place function.
     * 
     * @param placedBlock
     *            Placed Block
     * @param player
     *            Player
     * @param blockchange
     *            Change Block
     * @since 1.0.0
     */
    public BlockPlace(final CraftBlock placedBlock, final Player player, final BlockChange blockchange) {
        super(placedBlock, blockchange.getLocation().getBlock().getState(), placedBlock, player.getItemInHand(), player, true);
        bc = blockchange;
    }

    /**
     * Returns block change.
     *
     * @return Block change
     * @since 1.0.0
     */
    public final BlockChange getBlockChange() {
        return bc;
    }
}
