package net.diamondmine.mcftfill.block;

import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Player;

/**
 * Block change class.
 * 
 * @author Jon la Cour
 * @version 1.0.0
 */
public class BlockChange {
    private final int type;
    private final byte data;
    private final Location loc;
    private final Player player;

    /**
     * Block change function.
     * 
     * @param p
     *            Player
     * @param l
     *            Block location
     * @param t
     *            Block type
     * @param d
     *            Block data
     * @since 1.0.0
     */
    public BlockChange(final Player p, final Location l, final int t, final byte d) {
        loc = l;
        type = t;
        data = d;
        player = p;
    }

    /**
     * Block change function.
     * 
     * @param p
     *            Player
     * @param l
     *            Block location
     * @param t
     *            Block type
     * @since 1.0.0
     */
    public BlockChange(final Player p, final Location l, final int t) {
        loc = l;
        type = t;
        data = 0;
        player = p;
    }

    /**
     * Returns World.
     * 
     * @return World
     * @since 1.0.0
     */
    public final World getWorld() {
        return loc.getWorld();
    }

    /**
     * Returns Location.
     * 
     * @return Location
     * @since 1.0.0
     */
    public final Location getLocation() {
        return loc;
    }

    /**
     * Returns Type.
     * 
     * @return Type
     * @since 1.0.0
     */
    public final int getType() {
        return type;
    }

    /**
     * Returns Data.
     * 
     * @return Data
     * @since 1.0.0
     */
    public final byte getData() {
        return data;
    }

    /**
     * Returns Player.
     * 
     * @return Player
     * @since 1.0.0
     */
    public final Player getPlayer() {
        return player;
    }
}
