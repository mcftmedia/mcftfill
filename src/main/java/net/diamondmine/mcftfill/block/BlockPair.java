package net.diamondmine.mcftfill.block;

import net.diamondmine.mcftfill.McftFill;
import net.milkbowl.vault.permission.Permission;

import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Player;

/**
 * Block selection class.
 * 
 * @author Jon la Cour
 * @version 1.0.2
 */
public class BlockPair {
    private final Permission p;
    private Player player = null;
    private Location point1 = null;
    private Location point2 = null;

    /**
     * Block selection function.
     * 
     * @param p
     *            Player
     * @since 1.0.0
     */
    public BlockPair(final Player p) {
        player = p;
        this.p = McftFill.p;
    }

    /**
     * Verifies data.
     * 
     * @param t
     *            Location data
     * @return boolean
     */
    public final boolean rightMatch(final Location t) {
        if (point2 == null) {
            return false;
        }
        if ((t.getBlockX() == point2.getBlockX()) && (t.getBlockY() == point2.getBlockY()) && (t.getBlockZ() == point2.getBlockZ())) {
            return true;
        }
        return false;
    }

    /**
     * Returns Player.
     * 
     * @return Player
     * @since 1.0.0
     */
    public final Player getPlayer() {
        return player;
    }

    /**
     * Returns left click point Location.
     * 
     * @return Location
     * @since 1.0.0
     */
    public final Location getLeftClick() {
        return point1;
    }

    /**
     * Sets left click point Location.
     * 
     * @param loc
     *            Location
     * @since 1.0.0
     */
    public final void setLeftClick(final Location loc) {
        point1 = loc;
    }

    /**
     * Returns right click point Location.
     * 
     * @return Location
     * @since 1.0.0
     */
    public final Location getRightClick() {
        return point2;
    }

    /**
     * Sets right click point Location.
     * 
     * @param loc
     *            Location
     * @since 1.0.0
     */
    public final void setRightClick(final Location loc) {
        point2 = loc;
    }

    /**
     * Returns Location array of both left and right click point Locations.
     * 
     * @return Location array
     * @since 1.0.0
     */
    public final Location[] getLocs() {
        if (point1 == null || point2 == null) {
            return null;
        }
        Location[] locs = { point1, point2 };
        return locs;
    }

    /**
     * Gets the amount of blocks in an area.
     * 
     * @return Integer
     * @since 1.0.0
     */
    public final int getArea() {
        if (point1 == null || point2 == null) {
            return 0;
        }
        if (point1.getWorld() != point2.getWorld()) {
            return 0;
        }

        int x1 = Math.max(point1.getBlockX(), point2.getBlockX());
        int x2 = Math.min(point1.getBlockX(), point2.getBlockX());

        int y1 = Math.max(point1.getBlockY(), point2.getBlockY());
        int y2 = Math.min(point1.getBlockY(), point2.getBlockY());

        int z1 = Math.max(point1.getBlockZ(), point2.getBlockZ());
        int z2 = Math.min(point1.getBlockZ(), point2.getBlockZ());

        boolean unstable = false;
        int[] unstableIds = new int[] {6, 31, 32, 37, 38, 39, 40, 50, 51, 55, 59, 75, 76, 83, 90, 92, 95, 104, 105, 111, 115, 119};
        for (int unstableId : unstableIds) {
            if (point2.getBlock().getTypeId() == unstableId) {
                unstable = true;
            }
        }

        World world = point1.getWorld();
        int area = 0;

        if (p.has(player, "mcftfill.all")) {
            area = (x1 - x2 + 1) * (y1 - y2 + 1) * (z1 - z2 + 1);
        } else {
            for (int x = x2; x <= x1; x++) {
                for (int y = y2; y <= y1; y++) {
                    for (int z = z2; z <= z1; z++) {
                        Location loc = new Location(world, x, y, z);
                        if (rightMatch(loc) && unstable) {
                            area++;
                        }
                        if (loc.getBlock().isEmpty()) {
                            area++;
                        }
                    }
                }
            }
        }

        return area;
    }

    /**
     * Adds area to fill queue.
     * 
     * @param plugin
     *            The plugin class
     * @since 1.0.0
     */
    public final void queue(final McftFill plugin) {
        if (point1 == null || point2 == null) {
            return;
        }
        if (point1.getWorld() != point2.getWorld()) {
            return;
        }

        int type = point1.getBlock().getTypeId();
        byte data = point1.getBlock().getData();

        int x1 = Math.max(point1.getBlockX(), point2.getBlockX());
        int x2 = Math.min(point1.getBlockX(), point2.getBlockX());

        int y1 = Math.max(point1.getBlockY(), point2.getBlockY());
        int y2 = Math.min(point1.getBlockY(), point2.getBlockY());

        int z1 = Math.max(point1.getBlockZ(), point2.getBlockZ());
        int z2 = Math.min(point1.getBlockZ(), point2.getBlockZ());

        World world = point1.getWorld();

        for (int x = x2; x <= x1; x++) {
            for (int y = y2; y <= y1; y++) {
                for (int z = z2; z <= z1; z++) {
                    Location loc = new Location(world, x, y, z);
                    plugin.fillExecutor.add(type, data, loc, player);
                }
            }
        }
    }
}
