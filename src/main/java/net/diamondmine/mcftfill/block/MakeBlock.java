package net.diamondmine.mcftfill.block;

import org.bukkit.Material;
import org.bukkit.craftbukkit.v1_5_R3.CraftChunk;
import org.bukkit.craftbukkit.v1_5_R3.block.CraftBlock;

/**
 * Make Block class.
 * 
 * @author Jon la Cour
 * @version 1.0.0
 */
public class MakeBlock extends CraftBlock {
    public int id;

    /**
     * Make Block function.
     * 
     * @param chunk
     *            Chunk
     * @param x
     *            X Coordinate
     * @param y
     *            Y Coordinate
     * @param z
     *            Z Coordinate
     * @param idd
     *            Block id
     * @since 1.0.0
     */
    public MakeBlock(final CraftChunk chunk, final int x, final int y, final int z, final int idd) {
        super(chunk, x, y, z);
        id = idd;
    }

    /**
     * Returns the ID of a block.
     * 
     * @return Integer
     * @since 1.0.0
     */
    @Override
    public final int getTypeId() {
        return id;
    }

    /**
     * Returns the Material.
     * 
     * @return Material
     * @since 1.0.0
     */
    @Override
    public final Material getType() {
        return Material.getMaterial(id);
    }
}
