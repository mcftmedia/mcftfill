package net.diamondmine.mcftfill;

import org.bukkit.ChatColor;

/**
 * Handler for utilities.
 * 
 * @author Jon la Cour
 * @version 1.0.0
 */
public class Util extends McftFill {
    public static McftFill plugin;

    /**
     * This replaces all default color codes in text with Bukkit ChatColors.
     * 
     * @param m
     *            The message to fix
     * @return A string with Bukkit ChatColors
     * @since 1.0.0
     */
    public static String fixColor(final String m) {
        String s = m;
        s = s.replace("&4", ChatColor.DARK_RED + "");
        s = s.replace("&c", ChatColor.RED + "");
        s = s.replace("&6", ChatColor.GOLD + "");
        s = s.replace("&e", ChatColor.YELLOW + "");
        s = s.replace("&2", ChatColor.DARK_GREEN + "");
        s = s.replace("&a", ChatColor.GREEN + "");
        s = s.replace("&b", ChatColor.AQUA + "");
        s = s.replace("&3", ChatColor.DARK_AQUA + "");
        s = s.replace("&1", ChatColor.DARK_BLUE + "");
        s = s.replace("&9", ChatColor.BLUE + "");
        s = s.replace("&d", ChatColor.LIGHT_PURPLE + "");
        s = s.replace("&5", ChatColor.DARK_PURPLE + "");
        s = s.replace("&f", ChatColor.WHITE + "");
        s = s.replace("&7", ChatColor.GRAY + "");
        s = s.replace("&8", ChatColor.DARK_GRAY + "");
        s = s.replace("&0", ChatColor.BLACK + "");
        return s;
    }
}
