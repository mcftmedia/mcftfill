package net.diamondmine.mcftfill;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.logging.Logger;

import net.diamondmine.mcftfill.listeners.BlockSelect;
import net.diamondmine.mcftfill.listeners.Fill;
import net.milkbowl.vault.permission.Permission;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.plugin.PluginDescriptionFile;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * Core of McftFill.
 * 
 * @author Jon la Cour
 * @version 1.2.0
 */
public class McftFill extends JavaPlugin {
    public FillExecutor fillExecutor;
    public static final Logger logger = Logger.getLogger("Minecraft");
    public static HashSet<String> users = new HashSet<String>();
    public static Map<String, Integer> limits = new HashMap<String, Integer>();
    public static Permission p;

    /**
     * Plugin disabled.
     */
    @Override
    public final void onDisable() {
        fillExecutor.stopProcess();
        log("Version " + getDescription().getVersion() + " is disabled!");
    }

    /**
     * Plugin enabled.
     */
    @Override
    public final void onEnable() {
        // Permissions
        if (!setupPermissions()) {
            getServer().getPluginManager().disablePlugin(this);
            return;
        }

        // Settings
        config = getConfig();
        checkConfig();
        loadConfig();

        fillExecutor = new FillExecutor(this);
        fillExecutor.start();

        new Fill(this);
        new BlockSelect(this);

        PluginDescriptionFile pdfFile = getDescription();
        String version = pdfFile.getVersion();
        log("Version " + version + " enabled", "info");
        
        getServer().getScheduler().runTaskTimerAsynchronously(this, new Runnable() {
        	public void run() {
        		if (!fillExecutor.isAlive()) {
        			fillExecutor.stopProcess();
        			fillExecutor = new FillExecutor(new McftFill());
        			fillExecutor.start();
        			
        			log("Fill executor was frozen. It has been restarted.", "warning");
        		}
        	}
        }, 1200L, 1200L);
    }

    @Override
    public final boolean onCommand(final CommandSender sender, final Command cmd, final String commandLabel, final String[] args) {
        Player player = (Player) sender;
        String pname = player.getName();
        if (sender instanceof Player) {
            if (cmd.getName().equalsIgnoreCase("fill")) {
                String world = player.getWorld().getName();
                String group = p.getPrimaryGroup(world, pname).toLowerCase();
                if (limits.containsKey(group)) {
                    if (users.contains(pname)) {
                        users.remove(pname);
                        player.sendMessage(ChatColor.GOLD + "Fill disabled.");
                        return true;
                    } else {
                        users.add(pname);
                        String tool = Material.getMaterial(toolId).name().toLowerCase().replace("_", " ");
                        player.sendMessage(ChatColor.GOLD + "Fill enabled. Use a " + tool + " to select two points.");
                        return true;
                    }
                } else {
                    player.sendMessage(ChatColor.GOLD + "Your group is not allowed to fill!");
                    return true;
                }
            }
            if (cmd.getName().equalsIgnoreCase("mcftfill") && p.has(player, "mcftfill.config")) {
                if (args.length < 1) {
                    player.sendMessage(ChatColor.GOLD + "Not enough arguments!");
                    player.sendMessage(ChatColor.RED + "/mcftfill " + ChatColor.GRAY + "setting arguments " + ChatColor.YELLOW + "Changes a setting.");
                    player.sendMessage(ChatColor.YELLOW + "Available settings: " + ChatColor.GRAY + "groups, tool-id, max-size");
                    player.sendMessage(ChatColor.GRAY + "Example: " + ChatColor.YELLOW + "/mcftfill groups add Admin 500");
                    player.sendMessage(ChatColor.GRAY + "Example: " + ChatColor.YELLOW + "/mcftfill max-size 7000");
                    player.sendMessage(ChatColor.RED + "/mcftfill " + ChatColor.GRAY + "reload " + ChatColor.YELLOW + "Reloads settings.");
                }
                if (args.length == 1) {
                    if (args[0].equalsIgnoreCase("reload")) {
                        try {
                            loadConfig();
                            player.sendMessage(ChatColor.GOLD + "Configuration reloaded.");
                            log("Configuration reloaded by " + pname + ".", "info");
                        } catch (Exception e) {
                            player.sendMessage(ChatColor.GOLD + "Error reloading configuration file. Please see the console for more information.");
                        }
                    } else if (args[0].equalsIgnoreCase("groups") || args[0].equalsIgnoreCase("group")) {
                        player.sendMessage(ChatColor.GOLD + "Not enough arguments!");
                        player.sendMessage(ChatColor.RED + "- " + ChatColor.GRAY + "Example: " + ChatColor.YELLOW + "/mcftfill groups add Admin 500");
                        player.sendMessage(ChatColor.RED + "- " + ChatColor.YELLOW + "This allows the Admin group to fill - limited to 500 block fills.");
                        player.sendMessage(ChatColor.RED + "- " + ChatColor.GRAY + "Example: " + ChatColor.YELLOW + "/mcftfill groups add Owner -1");
                        player.sendMessage(ChatColor.RED + "- " + ChatColor.YELLOW + "This allows the Owner group to fill - with no limit.");
                        player.sendMessage(ChatColor.RED + "- " + ChatColor.GRAY + "Example: " + ChatColor.YELLOW + "/mcftfill groups remove Member");
                        player.sendMessage(ChatColor.RED + "- " + ChatColor.YELLOW + "This removes the ability to fill from the Member group.");
                    } else if (args[0].equalsIgnoreCase("max-size")) {
                        player.sendMessage(ChatColor.GOLD + "You need to provide an integer for the max size.");
                    } else if (args[0].equalsIgnoreCase("tool-id")) {
                        player.sendMessage(ChatColor.GOLD + "You need to provide an item ID.");
                    } else {
                        player.sendMessage(ChatColor.GOLD + "Unknown argument. See /mcftfill for help.");
                    }
                }
                if (args.length > 1) {
                    if (args[0].equalsIgnoreCase("max-size")) {
                        try {
                            int size = Integer.parseInt(args[1]);
                            maxSize = size;
                            config.set("max-size", size);
                            saveConfig();
                            player.sendMessage(ChatColor.GOLD + "Max size changed to " + size + " blocks.");
                        } catch (Exception e) {
                            player.sendMessage(ChatColor.GOLD + "Invalid integer for max size.");
                        }
                    }
                    if (args[0].equalsIgnoreCase("tool-id")) {
                        String item = "";
                        try {
                            int id = Integer.parseInt(args[1]);
                            Material type = Material.getMaterial(id);
                            toolId = type.getId();
                            config.set("tool-id", id);
                            saveConfig();
                            item = type.name().replace("_", " ").toLowerCase();
                            player.sendMessage(ChatColor.GOLD + "Tool changed to " + item + " (" + id + ").");
                        } catch (Exception e) {
                            player.sendMessage(ChatColor.GOLD + "Invalid block ID " + args[1] + ".");
                        }
                    }
                    if (args[0].equalsIgnoreCase("groups") || args[0].equalsIgnoreCase("group")) {
                        if (args.length == 2 || args.length == 3) {
                            if (args[1].equalsIgnoreCase("remove") || args[1].equalsIgnoreCase("delete")) {
                                try {
                                    String action = args[1].toLowerCase();
                                    action = Character.toUpperCase(action.charAt(0)) + action.substring(1);
                                    String group = args[2].toLowerCase();
                                    group = Character.toUpperCase(group.charAt(0)) + group.substring(1);
                                    boolean exists = config.isSet("groups." + group + ".limit");
                                    if (exists) {
                                        config.set("groups." + group + ".limit", 0);
                                        limits.remove(group);
                                        saveConfig();
                                        player.sendMessage(ChatColor.GOLD + action + "d '" + group + "' from fill permissions.");
                                    } else {
                                        player.sendMessage(ChatColor.GOLD + "That group doesn't have fill permissions yet, so it can't be " + action.toLowerCase() + "d!");
                                    }
                                } catch (Exception e) {
                                    player.sendMessage(ChatColor.GOLD + "Error saving configuration file. Please see the console for more information.");
                                }
                            } else {
                                player.sendMessage(ChatColor.GOLD + "Not enough arguments! See /mcftfill groups.");
                            }
                            return true;
                        } else if (args.length == 4) {
                            if (args[1].equalsIgnoreCase("add") || args[1].equals("update") || args[1].equalsIgnoreCase("change")) {
                                try {
                                    String action = args[1].toLowerCase();
                                    action = Character.toUpperCase(action.charAt(0)) + action.substring(1);
                                    String end = "fill permissions.";
                                    if (action.equals("Add")) {
                                        action = "Adde";
                                        end = "to fill permissions.";
                                    }
                                    int limit = Integer.parseInt(args[3]);
                                    String group = args[2];
                                    config.set("groups." + group + ".limit", limit);
                                    limits.put(group.toLowerCase(), limit);
                                    saveConfig();
                                    player.sendMessage(ChatColor.GOLD + action + "d '" + group + "' " + end);
                                } catch (Exception e) {
                                    player.sendMessage(ChatColor.GOLD + "Error saving configuration file. Please see the console for more information.");
                                }
                                return true;
                            } else {
                                player.sendMessage(ChatColor.GOLD + "Invalid argument, please use remove/delete/add/change/update.");
                                return true;
                            }
                        } else {
                            player.sendMessage(ChatColor.GOLD + "Too many arguments! See /mcftfill groups.");
                            return true;
                        }
                    } else {
                        player.sendMessage(ChatColor.GOLD + "Unknown command! See /mcftfill.");
                    }
                }
            }
            return false;
        } else {
            sender.sendMessage("This command is only for players.");
            return true;
        }
    }

    /**
     * Checks to make sure a permissions plugin has been loaded.
     * 
     * @return True if one is present, otherwise false.
     * @since 1.0.0
     */
    private boolean setupPermissions() {
        RegisteredServiceProvider<Permission> permissionProvider = getServer().getServicesManager().getRegistration(net.milkbowl.vault.permission.Permission.class);
        if (permissionProvider != null) {
            p = permissionProvider.getProvider();
        }
        return (p != null);
    }

    private FileConfiguration config;

    public int maxSize = 0;
    public int toolId = Material.BLAZE_ROD.getId();

    /**
     * Sets default configuration up if there isn't a configuration file
     * present.
     * 
     * @since 1.0.0
     */
    private void checkConfig() {
        config.options().copyDefaults(true);
        config.addDefault("max-size", 0);
        config.addDefault("tool-id", Material.BLAZE_ROD.getId());
        config.options().copyDefaults(true);
        saveConfig();
    }

    /**
     * Loads configuration.
     * 
     * @since 1.0.0
     */
    private void loadConfig() {
        maxSize = config.getInt("max-size");
        toolId = config.getInt("tool-id");
        if (config.isSet("groups")) {
            for (Entry<String, Object> entry : config.getConfigurationSection("groups").getValues(true).entrySet()) {
                String key = entry.getKey().trim().replaceAll(".limit", "");
                String value = entry.getValue().toString().trim();
                if (!value.contains("MemorySection")) {
                    try {
                        int val = Integer.parseInt(value);
                        limits.put(key.toLowerCase(), val);
                    } catch (Exception e) {
                        log("Invalid integer set for group '" + key + "' in configuration file.", "warning");
                        log("Disabling fills for '" + key + "' group until fixed.", "warning");
                    }
                }
            }
        }
    }

    /**
     * Sends a message to the logger.
     * 
     * @param s
     *            The message to send
     * @param type
     *            The level (info, warning, severe)
     * @since 1.0.0
     */
    public static void log(final String s, final String type) {
        String message = "[McftFill] " + s;
        String t = type.toLowerCase();
        if (t != null) {
            boolean info = t.equals("info");
            boolean warning = t.equals("warning");
            boolean severe = t.equals("severe");
            if (info) {
                logger.info(message);
            } else if (warning) {
                logger.warning(message);
            } else if (severe) {
                logger.severe(message);
            } else {
                logger.info(message);
            }
        }
    }

    /**
     * Sends a message to the logger.
     * 
     * @param s
     *            The message to send.
     */
    public static void log(final String s) {
        log(s, "info");
    }
}
