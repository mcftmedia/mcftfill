package net.diamondmine.mcftfill;

import static net.diamondmine.mcftfill.McftFill.log;

import java.util.HashSet;
import java.util.Iterator;

import net.diamondmine.mcftfill.block.BlockChange;
import net.diamondmine.mcftfill.block.MakeBlock;
import net.diamondmine.mcftfill.listeners.BlockPlace;
import net.milkbowl.vault.permission.Permission;

import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.craftbukkit.v1_5_R3.CraftChunk;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * Fill executor.
 * 
 * @author Jon la Cour
 * @version 1.2.0
 */
public class FillExecutor extends Thread {
    private final HashSet<BlockChange> blockChange;
    private JavaPlugin plugin;
    private final Permission p;
    private boolean running = true;

    /**
     * Fill executor.
     * 
     * @since 1.0.0
     */
    public FillExecutor(McftFill plugin) {
        this.p = McftFill.p;
        this.plugin = plugin;
        blockChange = new HashSet<BlockChange>();
    }

    /**
     * Adds fill to fill executor.
     * 
     * @param type
     *            Block type
     * @param data
     *            Block data
     * @param loc
     *            Block location
     * @param player
     *            Player
     * @since 1.0.0
     */
    public final void add(final int type, final byte data, final Location loc, final Player player) {
        blockChange.add(new BlockChange(player, loc, type, data));
    }

    /**
     * Stops the fill executor.
     * 
     * @since 1.0.0
     */
    public final void stopProcess() {
        running = false;
    }

    /**
     * @since 1.0.0
     */
    @Override
    public final void run() {
        while (running) {
            HashSet<BlockChange> bChange = new HashSet<BlockChange>();
            Iterator<BlockChange> bIterator = blockChange.iterator();
            while (bIterator.hasNext()) {
                BlockChange bNext = bIterator.next();
                World world = bNext.getLocation().getWorld();
                int id = bNext.getType();
                int x = bNext.getLocation().getBlockX();
                int y = bNext.getLocation().getBlockY();
                int z = bNext.getLocation().getBlockZ();
                CraftChunk chunk = (CraftChunk) world.getChunkAt(new Location(world, x, y, z));

                if (p.has(bNext.getPlayer(), "mcftfill.all")) {
                	BlockPlace event = new BlockPlace(new MakeBlock(chunk, x, y, z, id), bNext.getPlayer(), bNext);
                	
                    plugin.getServer().getPluginManager().callEvent(event);
                } else {
                    if (bNext.getLocation().getBlock().isEmpty()) {
                        plugin.getServer().getPluginManager().callEvent(new BlockPlace(new MakeBlock(chunk, x, y, z, id), bNext.getPlayer(), bNext));
                    }
                }
                
                bChange.add(bNext);
            }
            blockChange.removeAll(bChange);
            bChange.clear();
            try {
                sleep(20);
            } catch (InterruptedException e) {
                log("Sleep interupted.", "warning");
            }
        }
        interrupt();
    }
}
