# McftFill

McftFill is used to fill selected areas. Fill means to replace all the blocks in an area with a specified block; you may know of this as set or cuboid. You can have this set to use only items that a player has in their inventory, keeping everything fair on a survival server. You can specify the limit of blocks that a player can use when filling as well, to prevent any large fills or to give different ranks different limits.

This was made to suit the needs of the [Diamond Mine](http://diamondmine.net) server.

**[License](https://diamondmine.net/plugins/license)** -- **[Issues](https://mcftmedia.atlassian.net/issues/?jql=project=FILL)** --  **[Builds](https://mcftmedia.atlassian.net/builds/browse/FILL-FILL)**
-- **[Download](http://diamondmine.net/plugins/download/McftFill)** -- **[Bukkit Dev](http://dev.bukkit.org/server-mods/mcftfill/)**